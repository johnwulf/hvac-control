/*
 * HVACHoldState.h
 *
 *  Created on: Jul 30, 2011
 *      Author: john
 */

#ifndef HVACHOLDSTATE_H_
#define HVACHOLDSTATE_H_

#include "HVACControlState.h"
#include "HVAC.h"
#include "HVACConfig.h"

class HVACHoldState: public HVACControlState {
  public:
    HVACHoldState();
    ~HVACHoldState();

    HVACControlState::StateName setTemperature(float temperature);

    /**
     * Called on the next active state, just before the state is going to be made active due to a
     * state transition.
     *
     * Subclasses must call the super class implementation
     */
    HVACControlState::Error transitionFrom(HVACControlState::StateName state, time_t now);

  private:
    float _setPoint;
};

#endif /* HVACHOLDSTATE_H_ */
