#include <HVAC.h>


const int LOW_PIN = 6;
const int MED_PIN = 7;
const int HIGH_PIN = 8;
const int COOL_POWER_PIN = 5;
const int HEAT_POWER_PIN = 4;

HVAC hvac(HEAT_POWER_PIN, COOL_POWER_PIN, LOW_PIN, MED_PIN, HIGH_PIN, HVAC::HEAT);

void setup() {
  Serial.begin(9600);
}

void loop() {
  if(Serial.available() > 0) {
    int c = Serial.read();
    
    Serial.print("I received byte: " );
    Serial.println(c, DEC);
    
    switch(c) {
      case 112:  // p
        hvac.power();
        break;
        
      case 111: // o
        hvac.on();
        break;
        
      case 113: // q
        hvac.off();
        break;
        
      case 108: // l
        hvac.low();
        break;
        
      case 109: // m
        hvac.medium();
        break;
        
      case 104: // h
        hvac.high();
        break;
        
      case 116: // t
        if(hvac.getMode() == HVAC::HEAT) {
          hvac.setMode(HVAC::COOL);
        }
        else {
          hvac.setMode(HVAC::HEAT);
        }
        break;
    }
  } 
}
