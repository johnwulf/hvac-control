/*
 * HVACController.cpp
 *
 *  Created on: Jul 31, 2011
 *      Author: john
 */

#include "HVACController.h"

#include "HVACConfig.h"

HVACOffState HVACController::_offState;
HVACAutoState HVACController::_autoState;
HVACAdvanceState HVACController::_advanceState;
HVACHoldState HVACController::_holdState;

HVACController::HVACController() {
}

HVACController::~HVACController() {
}

int HVACController::initialize() {
  _currentStateName = (HVACControlState::StateName)HVACCFG.getLastState();

  _offState.initialize(&_hvac);
  _autoState.initialize(&_hvac);
  _advanceState.initialize(&_hvac);
  _holdState.initialize(&_hvac);

  return _hvac.initialize();
}

int HVACController::update() {
  beforeStateCall();
  return afterStateCall(getState(_currentStateName)->update(HVACConfig::rtc().getTime()));
}

int HVACController::on() {
  beforeStateCall();
  return afterStateCall(getState(_currentStateName)->on());
}

int HVACController::off() {
  beforeStateCall();
  return afterStateCall(getState(_currentStateName)->off());
}

int HVACController::advance() {
  beforeStateCall();
  return afterStateCall(getState(_currentStateName)->advance());
}

int HVACController::setTemperature(float temperature) {
  beforeStateCall();
  return afterStateCall(getState(_currentStateName)->setTemperature(temperature));
}

int HVACController::runProgram() {
  beforeStateCall();
  return afterStateCall(getState(_currentStateName)->runProgram());
}

int HVACController::hold() {
  beforeStateCall();
  return afterStateCall(getState(_currentStateName)->hold());
}

int HVACController::beforeStateCall() {
  // TODO: We need to do something with the millis here, as the normal millis will not properly
  //       work in power savings mode.
  HVACConfig::temperatureSensor().readSensor(millis());
}

int HVACController::afterStateCall(HVACControlState::StateName nextStateName) {
  HVACControlState *nextState;
  int error = 0;

  if(nextStateName != _currentStateName) {
    // Deal with state change
    nextState = getState(nextStateName);
    error = nextState->transitionFrom(_currentStateName, HVACConfig::rtc().getTime());
    if(error != HVACControlState::NO_ERROR) {
      _currentStateName = nextStateName;
      HVACCFG.setLastState(_currentStateName);
    }
    else {
      // TODO: We need to do something here
    }
  }
  // All state changes etc. have been done, so now update the HVAC
  getState(_currentStateName)->updateHVAC();
}



/**
 * This is used instead of an array. Don't think that takes less memory, but if
 * you take the array with 4 pointers plus the initialization into account,
 * it is probably the same.
 */
HVACControlState* HVACController::getState(HVACControlState::StateName name) {
  switch(name) {
    case HVACControlState::OFF:
      return &_offState;
    case HVACControlState::AUTO:
      return &_autoState;
    case HVACControlState::HOLD:
      return &_holdState;
    case HVACControlState::ADVANCE:
      return &_advanceState;
    case HVACControlState::ERROR:
      return &_offState;
    default:
      return &_offState;
  }
}
