/*
 * HVACAdvanceState.h
 *
 *  Created on: Jul 30, 2011
 *      Author: john
 */

#ifndef HVACADVANCESTATE_H_
#define HVACADVANCESTATE_H_

#include "HVACControlState.h"

class HVACAdvanceState: public HVACControlState {
  public:
    HVACAdvanceState();
    ~HVACAdvanceState();

    HVACControlState::StateName update(time_t now);
    HVACControlState::StateName setTemperature(float temperature);
    HVACControlState::StateName advance();


    /**
     * Called on the next active state, just before the state is going to be made active due to a
     * state transition.
     *
     * Subclasses must call the super class implementation
     */
    HVACControlState::Error transitionFrom(HVACControlState::StateName state, time_t now);

  private:
    HVACControlState::Error advanceTo(time_t time);

    time_t _nextSetPointChange;
    time_t _timeForSetPoint;
    float _currentSetPoint; // Not sure if we need that

};

#endif /* HVACADVANCESTATE_H_ */
