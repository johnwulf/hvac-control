/*
 * HVACAdvanceState.cpp
 *
 *  Created on: Jul 30, 2011
 *      Author: john
 */

#include "HVACAdvanceState.h"

#include <TemperatureManager.h>

HVACAdvanceState::HVACAdvanceState() : HVACControlState(HVACControlState::ADVANCE){
}

HVACAdvanceState::~HVACAdvanceState() {
}

HVACControlState::StateName HVACAdvanceState::update(time_t now) {

  // TODO:  We need some intelligence here eventually, using how long it last time took around the same time
  //        to change a degree or half a degree and what the current temperature is.
  if(now >= _nextSetPointChange) {
    return AUTO;
  }

  return getName();
}

HVACControlState::StateName HVACAdvanceState::setTemperature(float temperature) {
  // If the new temperature is different from the current, update it
  if(_currentSetPoint != temperature) {
    // TODO: we want to round this according to the mode? Winter use ceiling, summer use floor?
    _currentSetPoint = round(temperature);
    // TODO: Is there a better way to deal with the sensor to avoid reading it too often
    //       probably a good idea is to put the readSensor outside of the states
    //       Lets do that for now.
    // HVACConfig::temperatureSensor().readSensor();
    updateSetPoint(_currentSetPoint);
  }

  return getName();
}

HVACControlState::StateName HVACAdvanceState::advance() {
  Error error;

  // We move to the next set point. In theory, this could advance until the end of time
  error = advanceTo(_timeForSetPoint + 1);

  if(error != NO_ERROR) {
    // TODO: Return the ERROR state and implement overall error handling
    return ADVANCE;
  }

  return ADVANCE;
}


HVACControlState::Error HVACAdvanceState::transitionFrom(HVACControlState::StateName state, time_t now) {
  Error error;

  // This is the next set point change when we need to go back to auto state
  error = advanceTo(now);
  _nextSetPointChange = _timeForSetPoint;

  if(error == NO_ERROR) {
    error = HVACControlState::transitionFrom(state, now);
  }

  return error;
}


HVACControlState::Error HVACAdvanceState::advanceTo(time_t time) {
  Error error = NO_ERROR;

  _timeForSetPoint = TEMPMGR.nextSetPointChange(time);
  _currentSetPoint = TEMPMGR.getSetPointFor(_timeForSetPoint);
  updateSetPoint(_currentSetPoint);

  return error;
}
