/*
 HVAC.h - library for controlling a HVAC
*/

#ifndef __HVAC_H__
#define __HVAC_H__

#include <WProgram.h>
#include <Sensor.h>

class HVAC {
public:
  enum Mode { HEAT = 0, COOL = 1, UNKNOWN = 2 };

  HVAC();
  ~HVAC();
  
  // ---- Initialization and reset
  bool initialize(Mode mode = HVAC::UNKNOWN, int heatPowerPin = -1, int coolPowerPin = -1);
  bool isInitialized() { return (_coolPin >= 0 && (_heatPin >= 0)); };
  bool reset() { return initialize(_mode, _heatPin, _coolPin); };

  // ---- Power control
  bool power();
  bool power(bool powerOn);
  void on() { power(true); };
  void off() { power(false); };
  bool isOn();
  
  // ---- Mode Control
  void setMode(Mode mode);
  void detectMode();
  Mode getMode();
  
  void printDebug();

private:

  void initPins();
  void setPins();
  int senseCurrent();

  int8_t _coolPin;
  int8_t _heatPin;
  
  Mode _mode;
  bool _power;
};
#endif

