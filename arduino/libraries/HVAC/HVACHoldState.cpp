/*
 * HVACHoldState.cpp
 *
 *  Created on: Jul 30, 2011
 *      Author: john
 */

#include "HVACHoldState.h"

#include "HVACConfig.h"


HVACHoldState::HVACHoldState() : HVACControlState(HVACControlState::HOLD) {
  _setPoint = -1;
}

HVACHoldState::~HVACHoldState() {

}

HVACControlState::StateName HVACHoldState::setTemperature(float temperature) {

  // If the new temperature is different from the current, update it
  if(_setPoint != temperature) {
    // TODO: we want to round this according to the mode? Winter use ceiling, summer use floor?
    _setPoint = round(temperature);
    updateSetPoint(_setPoint);
  }

  return getName();
}


HVACControlState::Error HVACHoldState::transitionFrom(HVACControlState::StateName state, time_t now) {
  Error error = NO_ERROR;

  // Memorize the current temperature
  _setPoint = HVACConfig::temperatureSensor().getFloatValue();

  if(error == NO_ERROR) {
    error = HVACControlState::transitionFrom(state, now);
  }

  return error;
}
