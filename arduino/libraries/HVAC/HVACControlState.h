/*
 * HVACControlState.h
 *
 *  Created on: Jul 29, 2011
 *      Author: john
 */

#ifndef HVACCONTROLSTATE_H_
#define HVACCONTROLSTATE_H_

#include "HVAC.h"

#include <Time.h>

class HVACControlState {
  public:
    enum StateName {
      OFF = 0,
      AUTO,
      ADVANCE,
      HOLD,
      ERROR,
      MAX_STATES
    };

    enum Error {
      NO_ERROR = 0,
      INITIALIZATION_ERROR = 1,
      STATE_TRANSITION_ERROR = 2
    };

    HVACControlState(HVACControlState::StateName name) {
      _name = name;
      _active = false;
    };

    ~HVACControlState();

    int initialize(HVAC *hvac) {
      _hvac = hvac;
      return 0;
    };

    HVACControlState::StateName getName() { return _name; };
    bool isActive() { return _active; };
    HVAC & getHVAC() {return (*_hvac); };


    virtual HVACControlState::StateName update(time_t now);
    virtual HVACControlState::StateName on();
    virtual HVACControlState::StateName off();
    virtual HVACControlState::StateName advance();
    virtual HVACControlState::StateName setTemperature(float temperature);
    virtual HVACControlState::StateName runProgram();
    virtual HVACControlState::StateName hold();

    /**
     * Called on the next active state, just before the state is going to be made active due to a
     * state transition.
     *
     * Subclasses must call the super class implementation
     */
    virtual HVACControlState::Error transitionFrom(HVACControlState::StateName state, time_t now);

    virtual void updateHVAC();

    void updateSetPoint(float setpoint);

  private:
    static HVAC * _hvac;

    StateName _name;
    bool _active;
    // TODO: see what the warning about packed/unpacked is about? Pointer better?
    void advanceForTime();

};

#endif /* HVACCONTROLSTATE_H_ */
