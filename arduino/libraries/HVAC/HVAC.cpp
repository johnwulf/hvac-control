/*
  HVAC.cpp - library implementation for controlling a HVAC
*/

#include "HVAC.h"
#include "HVACConfig.h"

// Delay to let the HVAC settle after power is switched on or off
#define HVAC_SWITCH_DELAY 500 
#define MODE_SENSOR_THRESHOLD 10 // At least 0.25A have been detected


HVAC::HVAC() {
  _heatPin = -1;
  _coolPin = -1;

  _mode = UNKNOWN;
  _power = false;
}

HVAC::~HVAC() {
  off();
}

bool HVAC::initialize(Mode mode, int heatPowerPin, int coolPowerPin) {
  _heatPin = (heatPowerPin < 0) ? HVACConfig::instance.getHVACHeatPin() : heatPowerPin;
  _coolPin = (coolPowerPin < 0) ? HVACConfig::instance.getHVACCoolPin() : coolPowerPin;
  _mode = (mode == UNKNOWN) ? (Mode)HVACConfig::instance.getHVACMode() : mode;

  _power = false;


  initPins();
  setPins();

  return true;
}

bool HVAC::power() {
  if(isInitialized() && (_mode != UNKNOWN)) {
    _power = !_power;
    setPins();
    delay(HVAC_SWITCH_DELAY);
  }

  return _power;
}

bool HVAC::power(bool powerOn) {
  if((_power && (!powerOn)) || ((!_power) && powerOn)) {
    power();
  }

  return _power;
}

bool HVAC::isOn() {
  return _power;
}

HVAC::Mode HVAC::getMode() {
  return _mode;
}


void HVAC::setMode(Mode mode) {
  off();
  _mode = mode;
}

// The process is to turn on cool and detect
// the current going to the fan. Only if the 
// system is set to colling, the fan will turn on
// and a current will be detected. 
void HVAC::detectMode() {
  int currentOff;
  int currentOn;
  
  /*
  // Do this only if we have a sensor
  if(_modePowerPin > -1) {
    // Step 1: Save current state
    Mode tmpMode = _mode;
    FanLevel tmpFanLevel = _fanLevel;
    bool tmpPower = _power;
    
    // Step 2: Calibrate. Turn off power, turn on sensor, measure current
    off();
    
    // Turn sensor on and let it ramp up
    digitalWrite(_modePowerPin, HIGH);
    
    // if power was off, we can speed this up
    if(tmpPower) {
      // If we actually turned off the power, i.e. it was on, give it a standard
      // delay to settle.
      delay(HVAC_SWITCH_DELAY);
    }
    else {
      // This is just for the sensor to settle, probably not needed
      delay(5);
    }

    currentOff = senseCurrent();
    
    // Step 3: Set mode to cool and start the fan
    _mode = COOL;
    high();
    on();
    delay(HVAC_SWITCH_DELAY); // Give it some time to start
    
    currentOn = senseCurrent();
    
    // Step 4: derive mode
    if((currentOn - currentOff) > MODE_SENSOR_THRESHOLD) {
      _mode = COOL;
    }
    else {
      _mode = HEAT;
    }

    // Step 5: Reset everything
    // Turn sensor off as we are done; reset modes; and turn power off if it was off
    digitalWrite(_modePowerPin, LOW);
    _fanLevel = tmpFanLevel;

    if(tmpPower) {
      on();
    }
    else {
      off();
    }
  }
  */
}

// senseCurrent expects the sensor to be turned on.
int HVAC::senseCurrent() {
  /*
  int current = 0;
  int total = 0;
  int maxCurrent = 0;
  int minCurrent = 10000;
  const int numMeasurements = 20;
  
  // Let's take 20 measurements and average them
  for(int i = 0; i < numMeasurements; i++) {
    current = analogRead(_modeSensorPin);
    if(maxCurrent < current) {
      maxCurrent = current;
    }
    if(minCurrent > current) {
      minCurrent = current;
    }
    
    total += current;
    // Wait for 20 ms
    delay(20);
  }
  
  // Take out the outliers
  total -= (minCurrent + maxCurrent);
  
  return total / (numMeasurements - 2);
  */
  return 0;
}

// private methods
void HVAC::initPins() {
  pinMode(_coolPin, OUTPUT);
  pinMode(_heatPin, OUTPUT);
  digitalWrite(_coolPin, LOW);
  digitalWrite(_heatPin, LOW);
 }

void HVAC::setPins() {
  // Only if the HVAC is powered on we will
  // utilize the power level. Otherwise, we 
  // will use the off state.
  if((_power) && (_mode == HEAT)) {
    digitalWrite(_coolPin, LOW);
    digitalWrite(_heatPin, HIGH);
  }
  else if((_power) && (_mode == COOL)) {
    digitalWrite(_heatPin, LOW);
    digitalWrite(_coolPin, HIGH);
  }
  else {
    digitalWrite(_heatPin, LOW);
    digitalWrite(_coolPin, LOW);
  }
}

void HVAC::printDebug() {
  Serial.print(" power:");
  if(_power) {
    Serial.print("true");    
  } 
  else {
    Serial.print("false");        
  }
  Serial.print(", mode:");
  Serial.print(_mode);
  Serial.print(", coolPin:");
  Serial.print(_coolPin);
  Serial.print(", heatPin:");
  Serial.print(_heatPin);
}

