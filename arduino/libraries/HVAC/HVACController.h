/*
 * HVACController.h
 *
 *  Created on: Jul 31, 2011
 *      Author: john
 */

#ifndef HVACCONTROLLER_H_
#define HVACCONTROLLER_H_

#include <WProgram.h>

#include "HVAC.h"
#include "HVACControlState.h"
#include "HVACOffState.h"
#include "HVACAutoState.h"
#include "HVACAdvanceState.h"
#include "HVACHoldState.h"
#include "HVACConfig.h"

#include <DS1307RTC.h>

class HVACController {
  public:
    HVACController();
    ~HVACController();

    int initialize();

    HVACControlState::StateName getCurrentState() { return _currentStateName; };
    HVAC & getHVAC() { return _hvac; };


    int update();
    int on();
    int off();
    int advance();
    int setTemperature(float temperature);
    int runProgram();
    int hold();

  private:
    static HVACOffState _offState;
    static HVACAutoState _autoState;
    static HVACAdvanceState _advanceState;
    static HVACHoldState _holdState;

    HVACControlState* getState(HVACControlState::StateName name);
    int beforeStateCall();
    int afterStateCall(HVACControlState::StateName nextStateName);

    HVAC _hvac;
    HVACControlState::StateName _currentStateName;
};

#endif /* HVACCONTROLLER_H_ */
