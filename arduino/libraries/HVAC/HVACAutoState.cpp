/*
 * HVACAutoState.cpp
 *
 *  Created on: Jul 30, 2011
 *      Author: john
 */

#include "HVACAutoState.h"

#include <TemperatureManager.h>

HVACAutoState::HVACAutoState() : HVACControlState(HVACControlState::AUTO) {

}

HVACAutoState::~HVACAutoState() {
  // TODO Auto-generated destructor stub
}

HVACControlState::StateName HVACAutoState::update(time_t now) {

  // TODO:  We need some intelligence here eventually, using how long it last time took around the same time
  //        to change a degree or half a degree and what the current temperature is.
  if(now >= _nextSetPointChange) {
    // We passed the setpoint, so let's change the setpoint

    _currentSetPoint = TEMPMGR.getSetPointFor(now);
    _nextSetPointChange = TEMPMGR.nextSetPointChange(now);
    updateSetPoint(_currentSetPoint);
  }

  return getName();
}

HVACControlState::StateName HVACAutoState::setTemperature(float temperature) {
  // If the new temperature is different from the current, update it
  if(_currentSetPoint != temperature) {
    // TODO: we want to round this according to the mode? Winter use ceiling, summer use floor?
    _currentSetPoint = round(temperature);
    updateSetPoint(_currentSetPoint);
  }

  return getName();
}


HVACControlState::Error HVACAutoState::transitionFrom(HVACControlState::StateName state, time_t now) {
  Error error = NO_ERROR;

  _currentSetPoint = TEMPMGR.getSetPointFor(now);
  _nextSetPointChange = TEMPMGR.nextSetPointChange(now);
  updateSetPoint(_currentSetPoint);

  if(error == NO_ERROR) {
    error = HVACControlState::transitionFrom(state, now);
  }

  return error;
}
