/*
 * HVACOffState.h
 *
 *  Created on: Jul 30, 2011
 *      Author: john
 */

#ifndef HVACOFFSTATE_H_
#define HVACOFFSTATE_H_

#include "HVACControlState.h"

class HVACOffState: public HVACControlState {
  public:
    HVACOffState();
    ~HVACOffState();

    HVACControlState::StateName on();

    HVACControlState::Error transitionFrom(HVACControlState::StateName name, time_t now);

    /**
     * Turns the HVAC off, if it is not already off
     */
    void updateHVAC();
};

#endif /* HVACOFFSTATE_H_ */
