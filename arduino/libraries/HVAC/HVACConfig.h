/*
 * HVACConfig.h
 *
 *  Created on: Jul 27, 2011
 *      Author: john
 */

#ifndef HVACCONFIG_H_
#define HVACCONFIG_H_

#include <WProgram.h>
#include <Sensor.h>
#include <ACS712.h>
#include <SHT21.h>
#include <DS1307RTC.h>
#include <BBController.h>

#define MEM_BEGIN 0x00

// EEPROM Memory map
#define ID                 0 + MEM_BEGIN
#define MAJOR_VERSION      2 + MEM_BEGIN
#define MINOR_VERSION      3 + MEM_BEGIN

#define HVAC_BEGIN         4 + MEM_BEGIN
#define HVAC_HEAT_PIN      0 + HVAC_BEGIN
#define HVAC_COOL_PIN      1 + HVAC_BEGIN
#define HVAC_MODE          2 + HVAC_BEGIN
#define HVAC_LEN           3

#define RTC_BEGIN          HVAC_BEGIN + HVAC_LEN
#define RTC_TYPE           0 + RTC_BEGIN
#define RTC_VCC_PIN        1 + RTC_BEGIN
#define RTC_IRQ            2 + RTC_BEGIN
#define RTC_SQW_PIN        3 + RTC_BEGIN
#define RTC_LEN            3 + RTC_BEGIN

#define HT_BEGIN           RTC_BEGIN + RTC_LEN
#define HT_TYPE            0 + HT_BEGIN
#define HT_VCC_PIN         1 + HT_BEGIN
#define HT_SDA_PIN         2 + HT_BEGIN
#define HT_LEN             3 + HT_BEGIN

#define CS_BEGIN           HT_BEGIN + HT_LEN
#define CS_TYPE            0 + CS_BEGIN
#define CS_VCC_PIN         1 + CS_BEGIN
#define CS_ANALOG_PIN      1 + CS_BEGIN
#define CS_LEN             3 + CS_BEGIN


#define OTHER_BEGIN           CS_BEGIN + CS_LEN
#define MAX_PROFILE_COUNT     0 + OTHER_BEGIN
#define LAST_CONTROLLER_STATE 1 + OTHER_BEGIN
#define BASIC_CONFIG_END      2 + OTHER_BEGIN

#define TEMPERATURE_MGR_BEGIN BASIC_CONFIG_END
#define TEMPERATURE_MGR_LEN   40

// The temperature profile space is dynamic. It is calculated from the info stored in the EEPROM
#define TEMP_PROFILES_BEGIN   TEMPERATURE_MGR_BEGIN + TEMPERATURE_MGR_LEN
#define TEMP_PROFILE_LEN      16
#define TEMP_PROFILES_LEN     TEMP_PROFILE_LEN * getMaxProfileCount() // 16 Bytes per profile, 8 full day profiles, 2 profiles per day, 2 seasons

#define FREE_MEMORY_BEGIN TEMP_PROFILES_BEGIN + TEMP_PROFILES_LEN

#define EEPROM_MAX_MEMORY 1024

#define HVACCFG HVACConfig::instance

// TODO: Generalize this
// TODO: Calculate and write checksum
// TODO: Provide checksum verification
// TODO: Provide initial write and verification mode

class HVACConfig {
  public:
    enum DeviceType {
      SENSOR_SHT21 = 0,
      SENSOR_DHT22 = 1,
      SENSOR_ACS712 = 0,
      SENSORE_Si8505 = 1,
      RTC_DS1307 = 0,
      RTC_DS1341 = 1,
    };

    static HVACConfig instance;

    static Sensor& currentSensor() { return _currentSensor; };
    static Sensor& temperatureSensor() { return _temperatureSensor; };
    static Sensor& humiditySensor() { return _humiditySensor; };
    static DS1307RTC& rtc() { return DS1307RTC::instance; };
    static BBController& controller() { return _controller; };

    /**
     * Initializes all sensors and devices. This has to be done
     * as part of the setup in the main.
     */
    int initialize();
    int freeMemoryBegin() { return FREE_MEMORY_BEGIN; };

    // Hardware configuration
    uint16_t getDeviceID() { return readUInt(ID); };
    void setDeviceID(uint16_t id) { writeUInt(ID, id); };
    // Version
    byte getMajorVersion() { return readByte(MAJOR_VERSION); };
    void setMajorVersion(byte version) { writeByte(MAJOR_VERSION, version); };

    byte getMinorVersion() { return readByte(MINOR_VERSION); };
    void setMinorVersion(byte version) { writeByte(MINOR_VERSION, version); };

    // HVAC Control info
    uint8_t getHVACHeatPin() { return readByte(HVAC_HEAT_PIN); };
    void setHVACHeatPin(uint8_t pin) { writeByte(HVAC_HEAT_PIN, pin); };

    uint8_t getHVACCoolPin() { return readByte(HVAC_COOL_PIN); };
    void setHVACCoolPin(uint8_t pin) { writeByte(HVAC_COOL_PIN, pin); };

    uint8_t getHVACMode() { return readByte(HVAC_MODE); };
    void setHVACMode(uint8_t mode) { writeByte(HVAC_MODE, mode); };

    // RTC Info
    HVACConfig::DeviceType getRTCType() { return (DeviceType)readByte(RTC_TYPE); };
    void setRTCType(HVACConfig::DeviceType type) { writeByte(RTC_TYPE, (uint8_t)type); };

    uint8_t getRTCVccPin() { return readByte(RTC_VCC_PIN); };
    void setRTCVccPin(uint8_t pin) { writeByte(RTC_VCC_PIN, pin); };

    // This is redundant with IRQ number PIN = 2 + IRQ No (on UNO, different on MEGA)
    uint8_t getRTCIrqPin() { return readByte(RTC_SQW_PIN); };
    void setRTCIrqPin(uint8_t pin) { writeByte(RTC_SQW_PIN, pin); };

    uint8_t getRTCIrq() { return readByte(RTC_IRQ); };
    void setRTCIrq(uint8_t pin) { writeByte(RTC_IRQ, pin); };


    // Temp Sensor
    HVACConfig::DeviceType getHTType() { return (DeviceType)readByte(HT_TYPE); };
    void setHTType(HVACConfig::DeviceType type) { writeByte(HT_TYPE, (uint8_t)type); };

    uint8_t getHTVccPin() { return readByte(HT_VCC_PIN); };
    void setHTVccPin(uint8_t pin) { writeByte(HT_VCC_PIN, pin); };

    uint8_t getHTSDAPin() { return readByte(HT_SDA_PIN); };
    void setHTSDAPin(uint8_t pin) { writeByte(HT_SDA_PIN, pin); };

    // Current Sensor
    HVACConfig::DeviceType getCSType() { return (DeviceType) readByte(CS_TYPE); };
    void setCSType(HVACConfig::DeviceType type) { writeByte(CS_TYPE, (uint8_t)type); };

    uint8_t getCSVccPin() { return readByte(CS_VCC_PIN); };
    void setCSVccPin(uint8_t pin) { writeByte(CS_VCC_PIN, pin); };

    uint8_t getCSAnalogPin() { return readByte(CS_ANALOG_PIN); };
    void setCSAnalogPin(uint8_t pin) { writeByte(CS_ANALOG_PIN, pin); };

    // ------- Other configuration ------
    int getMaxProfileCount() { return readByte(MAX_PROFILE_COUNT); };
    bool setMaxProfileCount(uint8_t count);

    int getLastState() { return readByte(LAST_CONTROLLER_STATE); };
    void setLastState(uint8_t state) { writeByte(LAST_CONTROLLER_STATE, state); };

  private:
    HVACConfig();

    uint8_t readByte(int addr);
    void writeByte(int addr, uint8_t data);

    uint16_t readUInt(int addr);
    void writeUInt(int addr, uint16_t data);

    static ACS712 _currentSensor;
    static SHT21 _temperatureHumiditySensor;
    static SensorAdapter _temperatureSensor;
    static SensorAdapter _humiditySensor;
    static BBController _controller;
};

#endif /* HVACCONFIG_H_ */
