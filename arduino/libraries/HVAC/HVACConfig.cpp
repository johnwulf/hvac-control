/*
 * HVACConfig.cpp
 *
 *  Created on: Jul 27, 2011
 *      Author: john
 */

#include "HVACConfig.h"
#include <TemperatureProfileManager.h>
#include <TemperatureManager.h>
#include <Wire.h>
#include <EEPROM.h>


HVACConfig HVACConfig::instance;

// TODO: Generalize this
// TODO: Calculate and write checksum
// TODO: Provide checksum verification
// TODO: Provide initial write and verification mode

ACS712 HVACConfig::_currentSensor;
SHT21 HVACConfig::_temperatureHumiditySensor;

SensorAdapter HVACConfig::_temperatureSensor(&HVACConfig::_temperatureHumiditySensor, SHT21::TemperatureF);
SensorAdapter HVACConfig::_humiditySensor(&HVACConfig::_temperatureHumiditySensor, SHT21::Humidity);

BBController HVACConfig::_controller(BBController::INVERSE);

HVACConfig::HVACConfig() {

  // Setting memory info for temperature management configuration
  TemperatureManager::setMemoryInfo(TEMPERATURE_MGR_BEGIN);
  TemperatureProfileManager::setMemoryInfo(TEMP_PROFILES_BEGIN, getMaxProfileCount());
}

int HVACConfig::initialize() {
  // We need to initialize wire, so the sensors can be initialized
  Wire.begin();
  // TODO: Looking at this, it seems to be overkill to have the pin info in the EEPROM
  //       as that is static per configuration. The only advantage is that the
  //       release code doesn't have to change if any of that information changes
  //       Let's see if we need space and if we can do something more dynamic,
  //       like a factory that returns the appropriate type of sensor. In that way
  //       there, using different hardware and different pins is really just
  //       a question of changing the configuration in the EEPROM.

  // *********************** Current Sensor *************************
  _currentSensor.initialize(getCSAnalogPin(), getCSVccPin());

  // ****************** Temperature/Humidity Sensor *****************
  //TODO: Resolution should be in the EEPROM
  _temperatureHumiditySensor.setResolution(SHT21::RES_12_14);
  _temperatureHumiditySensor.initialize();

  // **************************** RTC *******************************
  // Configure the RTC
  DS1307RTC::instance.initialize(getRTCVccPin(), false);
  DS1307RTC::instance.start(); // But the clock should be already running
  //
  if((HVACCFG.getRTCIrqPin() >= 0) && (HVACCFG.getRTCIrq() >= 0)) {
    pinMode(HVACCFG.getRTCIrqPin(), INPUT);
    digitalWrite(HVACCFG.getRTCIrqPin(), HIGH);
  }

  // *********************** Controller *****************************
  //TODO: Controller mode should come from EEPROM
  //TODO: Tolerance should come from EEPROM
  _controller.setMode(BBController::INVERSE);
  _controller.setTolerance(1.0);


  // Need to fix the error. Need some error codes that indicate what might have gone wrong.
  return 0;
}

bool HVACConfig::setMaxProfileCount(uint8_t count) {
  if((count * TEMP_PROFILE_LEN + TEMP_PROFILES_BEGIN) < EEPROM_MAX_MEMORY) {
    TemperatureProfileManager::setMemoryInfo(TEMP_PROFILES_BEGIN, count);
    writeByte(MAX_PROFILE_COUNT, count);
    return true;
  }
  return false;
};


uint8_t HVACConfig::readByte(int addr) {
  return (uint8_t)EEPROM.read(addr);
}

void HVACConfig::writeByte(int addr, uint8_t data) {
  EEPROM.write(addr, data);
}

uint16_t HVACConfig::readUInt(int addr) {
  return readByte(addr) + (((uint16_t)readByte(addr)) << 8);
}

void HVACConfig::writeUInt(int addr, uint16_t data) {
  writeByte(addr, data & 0xFF);
  writeByte(addr + 1, data >> 8);
}
