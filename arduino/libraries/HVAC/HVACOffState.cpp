/*
 * HVACOffState.cpp
 *
 *  Created on: Jul 30, 2011
 *      Author: john
 */

#include "HVACOffState.h"

#include "HVACConfig.h"


HVACOffState::HVACOffState() : HVACControlState(HVACControlState::OFF){
}

HVACOffState::~HVACOffState() {

}

HVACControlState::StateName HVACOffState::on() {
  return AUTO;
}

HVACControlState::Error HVACOffState::transitionFrom(HVACControlState::StateName state, time_t now) {
  updateHVAC();

  return HVACControlState::transitionFrom(state, now);
}

void HVACOffState::updateHVAC() {
  getHVAC().off();
}


