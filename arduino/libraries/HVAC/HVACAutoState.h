/*
 * HVACAutoState.h
 *
 *  Created on: Jul 30, 2011
 *      Author: john
 */

#ifndef HVACAUTOSTATE_H_
#define HVACAUTOSTATE_H_

#include "HVACControlState.h"

class HVACAutoState: public HVACControlState {
  public:
    HVACAutoState();
    ~HVACAutoState();

    HVACControlState::StateName update(time_t now);
    HVACControlState::StateName setTemperature(float temperature);

    /**
     * Called on the next active state, just before the state is going to be made active due to a
     * state transition.
     *
     * Subclasses must call the super class implementation
     */
    HVACControlState::Error transitionFrom(HVACControlState::StateName state, time_t now);

  private:
    time_t _nextSetPointChange;
    float _currentSetPoint; // Not sure if we need that
};

#endif /* HVACAUTOSTATE_H_ */
