/*
 * HVACControlState.cpp
 *
 *  Created on: Jul 29, 2011
 *      Author: john
 */

#include "HVACControlState.h"

#include "HVACConfig.h"

HVAC* HVACControlState::_hvac;

HVACControlState::~HVACControlState() {
  // TODO Auto-generated destructor stub
}

HVACControlState::StateName HVACControlState::update(time_t now) {
  return _name;
}

HVACControlState::StateName HVACControlState::on() {
  return _name;
}

HVACControlState::StateName HVACControlState::off() {
  return OFF;
}

HVACControlState::StateName HVACControlState::advance() {
  return ADVANCE;
}

HVACControlState::StateName HVACControlState::setTemperature(float temperature) {
  return _name;
}

HVACControlState::StateName HVACControlState::runProgram() {
  return AUTO;
}

HVACControlState::StateName HVACControlState::hold() {
  return HOLD;
}

void HVACControlState::updateHVAC() {
  _hvac->power(HVACConfig::instance.controller().controlOn(HVACConfig::temperatureSensor().getFloatValue()));
}

void HVACControlState::updateSetPoint(float setpoint) {
  _hvac->power(HVACConfig::instance.controller().updateSetpoint(setpoint, HVACConfig::temperatureSensor().getFloatValue()));
}

/**
 * Called on the next active state, just before the state is going to be made active due to a
 * state transition.
 */
HVACControlState::Error HVACControlState::transitionFrom(HVACControlState::StateName state, time_t now) {
  _active = true;
  return NO_ERROR;
}

