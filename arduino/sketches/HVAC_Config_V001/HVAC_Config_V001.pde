#include <EEPROM.h>
#include <Wire.h>

#include <Time.h>

#include <Sensor.h>
#include <ACS712.h>
#include <SHT21.h>
#include <DS1307RTC.h>
#include <BBController.h>

#include <HVAC.h>

#include <TemperatureManager.h>
#include <TemperatureProfile.h>
#include <TemperatureProfileManager.h>

#include <HVACConfig.h>


#define MAJOR_VERSION 0
#define MINOR_VERSION 1

#define ID 0x4d81

#define cfg HVACConfig::instance

void setup() {
  Serial.begin(57600);
  Serial.print("Storing HVAC configuration ");
  Serial.print(MAJOR_VERSION);
  Serial.print(".0");
  Serial.print(MINOR_VERSION);
  Serial.print(" at ");
  Serial.print(MEM_BEGIN);
  Serial.print(" with length ");
  Serial.println(BASIC_CONFIG_END - MEM_BEGIN);
  
  // ----- BASIC configuration ------
  cfg.setDeviceID(ID);
  cfg.setMajorVersion(MAJOR_VERSION);
  cfg.setMinorVersion(MINOR_VERSION);
  // HVAC
  cfg.setHVACHeatPin(7);
  cfg.setHVACCoolPin(8);
  cfg.setHVACMode(HVAC::COOL); // Summer == Cooling
  
  // RTC
  cfg.setRTCType(HVACConfig::RTC_DS1307);
  cfg.setRTCVccPin(6);
  cfg.setRTCIrqPin(2); // This is actually redundant with IRQ number.
  cfg.setRTCIrq(0);
  
  // Temperature Sensor
  cfg.setHTType(HVACConfig::SENSOR_SHT21);
  cfg.setHTVccPin(0xff); // Not implemented in this version for SHT21
  cfg.setHTSDAPin(0xff); // Sits on I2C bus
  
  // Current Sensor
  cfg.setCSType(HVACConfig::SENSOR_ACS712);
  cfg.setCSVccPin(9);
  cfg.setCSAnalogPin(0);
  
  // 8 Full day profiles => 2 profiles per day per season => 8 * 2 * 2
  cfg.setMaxProfileCount(32);
  cfg.setLastState(0);

  Serial.print("Formating Temperature Profile space starting at ");
  Serial.println(TEMP_PROFILES_BEGIN);
  TPM.format();
  Serial.print("Clearing temperature manager at ");
  Serial.println(TEMPERATURE_MGR_BEGIN);
  TEMPMGR.clear();
  Serial.print("Total memory occupied: ");
  Serial.println(cfg.freeMemoryBegin() - MEM_BEGIN);
  
  // Define the profiles
  Serial.println("Defining profiles");
  // Workday AM, Summer 
  TPROFILE.setId(1);
  TPROFILE.add(4, 73);
  TPROFILE.add(12, 76);
  TPROFILE.add(16, 78);
  TPROFILE.add(46, 74);
  TPM.save();

  // Workday PM, Summer 
  TPROFILE.setId(2);
  TPROFILE.add(16, 73);
  TPROFILE.add(24, 71);
  TPROFILE.add(44, 72);
  TPM.save();
  
  // Weekend AM, Summer 
  TPROFILE.setId(3);
  TPROFILE.add(12, 74);
  TPM.save();

  // Weekend PM, Summer 
  TPROFILE.setId(4);
  TPROFILE.add(22, 73);
  TPROFILE.add(28, 71);
  TPM.save();
  
  
  // Assing the profiles
  Serial.println("Assigning profile");
  TEMPMGR.setProfile(3, TemperatureManager::SUNDAY, TemperatureManager::AM, TemperatureManager::SUMMER);
  TEMPMGR.setProfile(2, TemperatureManager::SUNDAY, TemperatureManager::PM, TemperatureManager::SUMMER);
  TEMPMGR.setProfile(1, TemperatureManager::MONDAY, TemperatureManager::AM, TemperatureManager::SUMMER);
  TEMPMGR.setProfile(2, TemperatureManager::MONDAY, TemperatureManager::PM, TemperatureManager::SUMMER);
  TEMPMGR.setProfile(1, TemperatureManager::TUESDAY, TemperatureManager::AM, TemperatureManager::SUMMER);
  TEMPMGR.setProfile(2, TemperatureManager::TUESDAY, TemperatureManager::PM, TemperatureManager::SUMMER);
  TEMPMGR.setProfile(1, TemperatureManager::WEDNESDAY, TemperatureManager::AM, TemperatureManager::SUMMER);
  TEMPMGR.setProfile(2, TemperatureManager::WEDNESDAY, TemperatureManager::PM, TemperatureManager::SUMMER);
  TEMPMGR.setProfile(1, TemperatureManager::THURSDAY, TemperatureManager::AM, TemperatureManager::SUMMER);
  TEMPMGR.setProfile(2, TemperatureManager::THURSDAY, TemperatureManager::PM, TemperatureManager::SUMMER);
  TEMPMGR.setProfile(1, TemperatureManager::FRIDAY, TemperatureManager::AM, TemperatureManager::SUMMER);
  TEMPMGR.setProfile(4, TemperatureManager::FRIDAY, TemperatureManager::PM, TemperatureManager::SUMMER);
  TEMPMGR.setProfile(3, TemperatureManager::SATURDAY, TemperatureManager::AM, TemperatureManager::SUMMER);
  TEMPMGR.setProfile(4, TemperatureManager::SATURDAY, TemperatureManager::PM, TemperatureManager::SUMMER);
  
  Serial.println("Chip has been configured .. Good bye");

}
  
  
void loop() {
  while(1) {
    delay(10000);
  }
}
